package controllers

import (
	"encoding/json"
	"net/http"
	"testApp1/helpers"
)

//Index route of test page
func Page1Index(w http.ResponseWriter, r *http.Request) {
	var result = helpers.Return{
		Data: "Page1",
	}

	json.NewEncoder(w).Encode(result)
}
