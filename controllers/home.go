package controllers

import (
	"encoding/json"
	"net/http"
	"testApp1/helpers"
)

//Index page of home
func HomeIndex(w http.ResponseWriter, r *http.Request) {
	var result = helpers.Return{
		Data: "Index",
	}

	json.NewEncoder(w).Encode(result)
}
