# GoLang Test app
A repository tartalma folyamatosan frissülni fog, ahogyan sikerül haladnom.

#### Kitűzött célok:
- MVC CRUD app létrehozása templatek használatával.
- Bootstrap, Fontawesome, jQuery stb. használatával egy layout -> partial -> page view rendszer kialakítása
- ~~API létrehozása~~
- MariaDB-ből adatok listázása (lista és egy adat megjelentése) és adatok írása adatbázisba
- Form validáció megvalósítása
- Email küldés megvalósítása
- Excel-ből adatok beolvasása és adatbázisból Excel generálása
- ~~Logolás megoldása~~
- Ajax lista készítése
- Külső API-ból adatok lekérdezése
- PDF generálása HTML alapján
- CSV fileból adat olvasása és CSV-be adatok mentése db-ből
- File fel- és letöltés megvalósítása
- Autentikáció megvalósítása
- JWT autentikáció megvalósítása
- Raw SQL parancsok használatának kipróbálása
- Json response formatter készítése
- ...
> A megvalósított célok át vannak húzva!
#### Tudnivalók:
- work in progress... :P