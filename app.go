package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"testApp1/controllers"

	log "github.com/sirupsen/logrus"
)

// User object
type User struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Age       int    `json:"age"`
}

func main() {
	//Log to console on app start.
	fmt.Printf("Listening on port: %v \n", 8080)

	var test string = "test"
	test = "akarmi"

	fmt.Printf("%s", test)

	http.HandleFunc("/decode", func(w http.ResponseWriter, r *http.Request) {
		var user User
		json.NewDecoder(r.Body).Decode(&user)

		fmt.Fprintf(w, "%s %s is %d years old!", user.FirstName, user.LastName, user.Age)
	})

	http.HandleFunc("/encode", func(w http.ResponseWriter, r *http.Request) {
		log.Info("eeeee")
		peter := User{
			FirstName: "John",
			LastName:  "Doe",
			Age:       25,
		}

		json.NewEncoder(w).Encode(peter)
	})

	http.HandleFunc("/home", controllers.HomeIndex)
	http.HandleFunc("/page1", controllers.Page1Index)

	http.ListenAndServe(":8080", nil)
}
